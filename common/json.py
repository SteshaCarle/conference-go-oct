# where custom encoders will exist
from json import JSONEncoder
from datetime import datetime
from django.db.models import QuerySet

class QuerySetEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, QuerySet):
            return list(o)
        else:
            return super().default(o)


class DateEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime):
            return o.isoformat()
        else:
            return super().default(o)


class ModelEncoder(DateEncoder, QuerySetEncoder, JSONEncoder):
    encoders = {}
    def default(self, o):
        # isinstance returns a boolean
        # self.model is calling the model from our encoder
        if isinstance(o, self.model):
            d = {}
            # if o has the attribute get_api_url
            if hasattr(o, "get_api_url"):
            #    then add its return value to the dictionary
            #    with the key "href"
                d["href"] = o.get_api_url()
            # properties coming from encoder
            for property in self.properties:
                value = getattr(o, property)
                if property in self.encoders:
                    encoder = self.encoders[property]()
                    value = encoder.default(value)
                d[property] = value
            d.update(self.get_extra_data(o))
            return d
        else:
            # calling default fn that lives on the super class (JSONEncoder)
            # there is a default method on that class
            return super().default(o)

    def get_extra_data(self, o):
        return {}
